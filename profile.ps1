#
# Here is my powershell profile :)
#


# Find out from git which branch we're in
function git_branch
{
    $branch = (git branch) | where-object { $_.startswith("* ") }
    if ($branch)
    {
         return $branch.substring(2)
    }
    return ""
}
 

# Get a useful prompt
function prompt
{
    $hostname = ""
    if ($IsOSX -or $IsLinux) {
        $hostname = (hostname).Replace(".local", "") + ": "
    }

    $path = $pwd.Path.Replace($HOME, "~")
    $branch = git_branch
    if ($branch -ne "")
    {
        $branch = " @"+$branch
    }

    "[$hostname$path$branch] "
}


# Give me some comfortable settings...
Set-Alias ~ $HOME
Set-Alias ll Get-ChildItem


# Load /etc/paths into $env:PATH
if ($IsOSX -and (Test-Path /etc/paths))
{
    $env:PATH = (get-content /etc/paths) -join ":"
    
    if (Test-Path /etc/paths.d)
    {
        $env:PATH += (get-childitem /etc/paths.d | ForEach-Object { get-content $_.FullName }) -join ":"
    } 
}


$local_settings = "$env:HOME/.powershell_local.ps1"

if (Test-Path $local_settings)
{
    #echo "Loading local settings in: $local_settings"
    . $local_settings
}